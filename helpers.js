const {MSGLIMIT,MOREFLAG}=require('./constants')

const promiseSetTimeOut = (time) => {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve();
    }, time);
  });
};

// will divide the message to slices, with slice of maximum length of MSGLIMIT
const divideMessage=(message="")=>{
  let i=0;
  const answers=[]
  while(i<message.length){

    let answer=""
    let lastIndex= i+MSGLIMIT-1
  
    // if remaining of message length is more than message limit then again MORE flag has to be added
    if(lastIndex -1  < message.length ){
      answer+=MOREFLAG

      // if msg limit is 2000 then from index 0 to 1999 but we also included flag too in beginning
      // so we have to subtract length of flag from slice too.
      lastIndex= i + MSGLIMIT-1- MOREFLAG.length
      

    }

    else{
      // if message length is less than limit then lastindex with 
      lastIndex= i + message.length-1;

    }
    
     
          // +1 at last because index in 2nd parameter of slice  dont include that index only upto
          answer+=message.slice(i,lastIndex+1)

          answers.push(answer);

    i=lastIndex+1;


  }

  return answers;

}

// will extract records with database on basis of keywords
const getRecordsWithKeywords=async (keywords="",pgClient)=>{
  const keywordArray=keywords.split(" ")
  let searchQuery=`select * from promptlogs
  where 
  `
  for(let keyword of keywordArray){
    searchQuery+=` question like '%${keyword}%' and`
  }

  // removing and from last bcz for last comparision and wouldnt be there
  // as there no more operations needs to be performed
  searchQuery=searchQuery.slice(0,searchQuery.length - 'and'.length)
  console.log('searchQuery: ',searchQuery)

  const res=await pgClient.query(searchQuery)
  
  console.log('res rows: ',res.rows)

  return res.rows;

}


const pretifyRecords=(records)=>{
  let str="\n"
  for(let record of records)
  {
    str+=`**question** : ${record.question}\n**answerlink** : ${record.answerlink}\n\n`
  }

  return str;
}

function generateRandomString(length) {
  const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  let result = '';
  const charactersLength = characters.length;
  for (let i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}


module.exports={promiseSetTimeOut,divideMessage,getRecordsWithKeywords,generateRandomString,pretifyRecords}
