const MSGLIMIT=2000;

// tells that there is more messages to be sent after sending current one
const MOREFLAG='[MORE]\n'

IS_DB_ENABLED=true

module.exports={MSGLIMIT,MOREFLAG,IS_DB_ENABLED}
