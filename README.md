# TruthGPT

A discord bot which acts as gpt bot like chatgpt.
&nbsp;

## Few things
- It is made with nodejs and posgresql database is used to store previous answers
- By default answers will be saved in database when using `.gpt`. So to turn off that by default, go to `constants.js` file and assign false to `IS_DB_ENABLED`.
- Make sure to create table named 'promptlogs'
    ```sql
    create table promptlogs(
    id serial primary key   not null,
    question text  not null,
    answerlink text  not null
    );

    ```
&nbsp;

**Eg**

`.gpt what is bot` : to simply get results

`.gptdb bot` : to get previous answers with question containing keywords.

`.gptnodb what is bot` : to not put results in database
