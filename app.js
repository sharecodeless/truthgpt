const { promiseSetTimeOut, divideMessage, getRecordsWithKeywords, pretifyRecords } = require("./helpers.js");

const { MSGLIMIT, MOREFLAG, IS_DB_ENABLED } = require("./constants.js");
const {uploadRecord}=require('./uploadRecord.js')

const { Client, GatewayIntentBits } = require("discord.js");
const fetch = require("node-fetch");
const pg = require("pg");
// const conString="postgres://YourUserName:YourPassword@localhost:5432/YourDatabase";



const fs = require("fs");

let answers = [];

const client = new Client({
  intents: [
    GatewayIntentBits.Guilds,
    GatewayIntentBits.GuildMessages,
    GatewayIntentBits.MessageContent,
  ],
});



let i = 0;

const dotenv = require("dotenv");
dotenv.config();
let pgClient

const createPGConnection=()=>{
  pgClient=new pg.Client(conString);
  
  pgClient.connect()
  console.log('database connected')

}

const conString = process.env.CONNECTION_URI;


client.on("ready", async () => {
  console.log(`Logged in as ${client.user.tag}!`);
  
  // const res1 = await pgClient.query("select * from promptlogs; ");
  // console.log(res1.rows);

  // await pgClient.end()
});

createPGConnection()

// Reconnecting in case database connection is closed
pgClient.on('error',async(err)=>{
  console.log('err: ',err)
  await pgClient.end()
  console.log('connection ended')
  console.log('trying to reconnect')
  await promiseSetTimeOut(5000) 
  await  createPGConnection()
})






client.on("messageCreate", async (msg) => {
  try {
    // Check if the message is from a user and not a bot
    if (msg.author.bot) {
      if (msg.content.startsWith(MOREFLAG)) {
        await promiseSetTimeOut(2000);
        msg.reply(answers[i++]);
      } else {
        // Resetting index and array if there is no more parts of message to be delivered
        i = 0;
        answers = [];
      }

      return;
    }
    console.log("got msg");
    if (msg.content.slice(0, 5) === ".gpt "  || msg.content.slice(0, 9) === ".gptnodb ") {
      console.log("got ping");
      
      let questionreg;

      if(msg.content.slice(0, 5) === ".gpt "){
           questionreg = /\.gpt\s+([\s\S]*)/;
      }
      else if(msg.content.slice(0, 9) === ".gptnodb "){
        questionreg = /\.gptnodb\s+([\s\S]*)/;

      }

      
    
      const question = questionreg.exec(msg)?.[1];
      let reply = "No reply";

      if (!question) {
        reply = "question field is empty";
      } else {
        const fetchurl = `${process.env.GPT_URL}${question}`;

        const res = await fetch(fetchurl);
        const msg1 = await res.text();

        
        //  fs.writeFileSync("answers_temp.md", msg1);
       /*  const msg1=fs.readFileSync('answers_temp.md') */

        // if message content doesnt start with '.gptnodb' then data wont be saved in db
        if (msg.content.slice(0, 8) != ".gptnodb") {

          uploadRecord(question,msg1)
          .then(async(answerlink)=>{
            const insertQuery = `insert into promptlogs (question,answerlink) values($1,$2)` 
            const values=[question,answerlink]
            await pgClient.query(insertQuery,values);

          })
        
        

        }

        fs.writeFileSync("answers.md", msg1);

        // dividing answers into slices using the function named 'divideMessage'
        // console.log('msg1: ',msg1)
        answers = divideMessage(msg1);
        reply = answers[i++];
      }

      console.log("question: ", question);
      await promiseSetTimeOut(1000);
      msg.reply(reply);
    } else if (msg.content.startsWith(".gptdb", 0) && IS_DB_ENABLED) {
      console.log("checking in db");
      const keywordsRegex= /\.gptdb\s+([\s\S]*)/;
      const keywords=keywordsRegex.exec(msg.content)[1];
      
     const records= await getRecordsWithKeywords(keywords,pgClient)
      
      // checking if there r any records found or not
      if(records?.length> 0){
        // dividing answers into slices using the function named 'divideMessage'
        answers = divideMessage(pretifyRecords(records));
        reply = answers[i++];
   
        
         await promiseSetTimeOut(1000);
      msg.reply(reply);

      }
      else{
        await promiseSetTimeOut(1000);
        msg.reply("No records found");

      }
      
                // console.log('msg1: ',msg1)
      //
      // console.log('records: ',records)
      // console.log('json records: ',JSON.stringify(records))     
       

    }
  } catch (err) {
    console.log("err: ", err);
  }
});

client.login(process.env.TOKEN);
