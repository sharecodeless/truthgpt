const fetch=require('node-fetch')

const {generateRandomString} = require('./helpers')

const baseurl='https://sentrytwo.com'

// uploading records in text hosting services
const uploadRecord=async(question,answer)=>{
  const fetchurl=`${baseurl}/api/new`

  const content=`## Question
  ${question}
  &nbsp;
  &nbsp;

  ## Answer
  ${answer}

  `
  
  const jsbody={
    content,
    custom_url : '',
    edit_password: generateRandomString(10),

  }
  try{


    const res= await fetch(fetchurl, {
      method: 'POST',
      headers : {
        'Content-Type': 'application/json'
      },
      body : JSON.stringify(jsbody)
    })

    const resObject=await res.json()

    console.log('resObject: ',resObject)

    const answerlink=`${baseurl}/${resObject.payload.custom_url}`

    return answerlink
  }
  catch(err){
    console.log('upload err: ',err)
    return "error"
  }

  // const answerlink=
}

module.exports={uploadRecord}
